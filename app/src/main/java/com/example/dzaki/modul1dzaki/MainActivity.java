package com.example.dzaki.modul1dzaki;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText z, x;
    Button hitung;
    TextView hasil1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        z = (EditText) findViewById(R.id.Alas2);
        x = (EditText) findViewById(R.id.tinggi2);
        hitung = (Button) findViewById(R.id.button1);
        hasil1 = (TextView) findViewById(R.id.hasil1);
        hitung.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    String insertalas = z.getText().toString();
                    String inserttinggi = x.getText().toString();
                    double z = Double.parseDouble(insertalas);
                    double x = Double.parseDouble(inserttinggi);
                    double hs = LuasLahan(z, x);
                    String output = String.valueOf(hs);
                    hasil1.setText(output.toString());
                }

        });
    }

    public double LuasLahan(double z, double x) {
        return z * x;
    }
}